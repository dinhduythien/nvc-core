<?php

namespace SCart\Core\Api\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use SCart\Core\Front\Models\ShopCustomer;
use SCart\Core\Front\Models\ShopEmailTemplate;
use SCart\Core\Front\Controllers\Auth\AuthTrait;
use SCart\Core\Library\DeviceTracking;
use Validator;

class AuthController extends BaseApiController
{
    use AuthTrait;

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'login' => 'required|string',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = $this->credentials($request);

        if(!Auth::attempt($credentials)){
            return $this->prepareResult($this->unauthenticated, [], [], "Unauthorized");
        }

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        if ($request->remember_me){
            $token->expires_at = Carbon::now()->addWeeks(1);
        }
        $token->save();

        $deviceId = $request->header('DEVICE-ID', '');
        if (!empty($deviceId)) {
            $ldt = device_tracking($request);
            $currentDevice = $ldt->findCurrentDevice($deviceId, true, true);
            $ldt->findOrSaveDevice($currentDevice, $user, false, true);
        }

        return $this->prepareResult($this->successStatus, [
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ], [], "Login successfully");
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  Request  $request
     * @param  string  $passwordKey
     *
     * @return array
     */
    protected function credentials(Request $request, string $passwordKey = 'password')
    {
        $password = $request->get($passwordKey, '');
        if(is_numeric($request->get('login'))){
            return ['phone'=>$request->get('login'),'password' => $password];
        }
        elseif (filter_var($request->get('login'), FILTER_VALIDATE_EMAIL)) {
            return ['email' => $request->get('login'), 'password' => $password];
        }
        return ['email' => $request->get('login'), 'password' => $password];
    }

    /**
     * Create new customer
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $data = $request->all();
        $data['country'] = strtoupper($data['country'] ?? 'VN');
        $v = $this->validator($data);
        if ($v->fails()) {
            $errors = [];
            foreach ($v->errors()->toArray() as $key => $value) {
                $errors[] = ["field" => $key, "message" => $value[0]];
            }
            return $this->prepareResult($this->badRequest, [], $errors, "Error while creating new customer");
        }
        $user = $this->insert($data);
        $deviceId = $request->header('DEVICE-ID', '');
        if (!empty($deviceId)) {
            $ldt = device_tracking($request);
            $currentDevice = $ldt->findCurrentDevice($deviceId, true, true);
            $ldt->findOrSaveDevice($currentDevice, $user, true, false);
        }
        return response()->json($user);
    }

    /**
     * Validate data input
     */
    protected function validator(array $data)
    {
        $dataMapping = $this->mappingValidator($data);
        return Validator::make($data, $dataMapping['validate'], $dataMapping['messages']);
    }

    /**
     * Insert data new customer
     *
     * @param $data
     *
     * @return mixed
     */
    protected function insert($data)
    {
        $dataMap = $this->mappDataInsert($data);

        $user = ShopCustomer::createCustomer($dataMap);
        if ($user) {
            if (sc_config('welcome_customer') && !empty($user->email)) {

                $checkContent = (new ShopEmailTemplate)->where('group', 'welcome_customer')->where('status', 1)->first();
                if ($checkContent) {
                    $content = $checkContent->text;
                    $dataFind = [
                        '/\{\{\$title\}\}/',
                        '/\{\{\$first_name\}\}/',
                        '/\{\{\$last_name\}\}/',
                        '/\{\{\$email\}\}/',
                        '/\{\{\$phone\}\}/',
                        '/\{\{\$password\}\}/',
                        '/\{\{\$address1\}\}/',
                        '/\{\{\$address2\}\}/',
                        '/\{\{\$address3\}\}/',
                        '/\{\{\$country\}\}/',
                    ];
                    $dataReplace = [
                        sc_language_render('email.welcome'),
                        $dataMap['first_name'],
                        $dataMap['last_name'],
                        $dataMap['email'],
                        $dataMap['phone'],
                        $dataMap['password'],
                        $dataMap['address1'],
                        $dataMap['address2'],
                        $dataMap['address3'],
                        $dataMap['country'],
                    ];
                    $content = preg_replace($dataFind, $dataReplace, $content);
                    $dataView = [
                        'content' => $content,
                    ];

                    $config = [
                        'to' => $data['email'],
                        'subject' => sc_language_render('email.welcome'),
                    ];

                    sc_send_mail($this->templatePath . '.mail.welcome_customer', $dataView, $config, []);
                }

            }
        }
        return $user;
    }


    /**
     * Logout user (Revoke the token)
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        $request->user()->token()->revoke();
        return response()->json([
            'error' => 0,
            'msg' => 'Successfully logged out'
        ]);
    }
  
}