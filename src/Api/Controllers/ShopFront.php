<?php
namespace SCart\Core\Api\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SCart\Core\Front\Models\ShopBrand;
use SCart\Core\Front\Models\ShopCategory;
use SCart\Core\Front\Models\ShopProduct;
use SCart\Core\Front\Models\ShopSupplier;

class ShopFront extends BaseApiController
{
    /**
     * display list category root (parent = 0)
     * @return JsonResponse [json]
     */
    public function allCategory(Request $request): JsonResponse
    {
        $query = ShopCategory::with('descriptions');
        if ($request->get('include_children', false)) {
            $query = $query->with('children');
        }
        $isHot = $request->get('is_hot', false);
        if ($isHot) {
            $query = $query->where('is_hot', true);
        }

        $itemsList = $query->jsonPaginate();

        return $this->prepareResult($this->successStatus, $itemsList, [], "OK");
    }

    /**
     * Category detail: list category child
     * @param  [int] $id
     * @return [json]
     */
    public function categoryDetail($id)
    {
        $category = (new ShopCategory)
            ->with('descriptions')
            ->find($id);
        if ($category) {
            return $this->prepareResult($this->successStatus, $category, [], "OK");
        } else {
            return $this->prepareResult($this->notFound, [], ["message" => "Resource not found"], "Not found");
        }
    }

    /**
     * All products
     * @return [json]
     */
    public function allProduct()
    {
        $products = (new ShopProduct)
            ->with('images')
            ->with('descriptions')
            ->with('promotionPrice')
            ->with('attributes')
            ->jsonPaginate();
        return $this->prepareResult($this->successStatus, $products, [], "OK");
    }

    /**
     * product detail
     * @param  [int] $id
     * @return [json]
     */
    public function productDetail($id)
    {
        $product = (new ShopProduct)
        ->with('images')
        ->with('descriptions')
        ->with('promotionPrice')
        ->with('attributes')
        ->find($id);
        if ($product) {
            return $this->prepareResult($this->successStatus, $product, [], "OK");
        } else {
            return $this->prepareResult($this->notFound, [], ["message" => "Resource not found"], "Product not found");
        }
    }

    public function allBrand()
    {
        $itemsList = (new ShopBrand)
            ->jsonPaginate();
        return $this->prepareResult($this->successStatus, $itemsList, [], "OK");
    }

    public function brandDetail($id)
    {
        $brand = (new ShopBrand)->find($id);
        if($brand) {
            return $this->prepareResult($this->successStatus, $brand, [], "OK");
        } else {
            return $this->prepareResult($this->notFound, [], ["message" => "Resource not found"], "Brand not found");
        }
    }

    public function allSupplier()
    {
        $itemsList = (new ShopSupplier)->jsonPaginate();
        return $this->prepareResult($this->successStatus, $itemsList, [], "OK");
    }

    public function supplierDetail($id)
    {
        $supplier = (new ShopSupplier)->find($id);
        if ($supplier) {
            return $this->prepareResult($this->successStatus, $supplier, [], "OK");
        } else {
            return $this->prepareResult($this->notFound, [], ["message" => "Resource not found"], "Supplier not found");
        }
    }
}