<?php

namespace SCart\Core\Api\Controllers;

use Illuminate\Http\Request;
use SCart\Core\Front\Models\ShopOrder;

class AccountController extends BaseApiController
{

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return $this->prepareResult($this->successStatus, $request->user(), [], "success");
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function orders(Request $request)
    {
        return $this->prepareResult($this->successStatus, $request->user()->orders, [], "success");
    }

    /**
     * Get order detail
     *
     * @return [json] user object
     */
    public function ordersDetail(Request $request, $id)
    {
        $id = (int)$id;
        $user = $request->user();
        $order = (new ShopOrder)->where('id', $id)
                ->with('details')
                -> where('customer_id', $user->id)
                ->first();
        if($order) {
            $dataReturn = $order;
        } else {
            return $this->prepareResult($this->notFound, [], ['message' => 'Order not found or no permission!',], "Not found");
        }
        return $this->prepareResult($this->successStatus, $dataReturn, [], "success");
    }

}