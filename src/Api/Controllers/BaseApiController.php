<?php

namespace SCart\Core\Api\Controllers;

use App\Http\Controllers\RootFrontController;
use Illuminate\Http\JsonResponse;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class BaseApiController extends RootFrontController
{
    public int $successStatus = 200;
    public int $unauthenticated = 401;
    public int $badRequest = 400;
    public int $notFound = 404;

    /**
     * Display a listing of the resource.
     *
     * @param $status
     * @param $data
     * @param $errors
     * @param $msg
     *
     * @return JsonResponse
     */
    protected function prepareResult($status, $data, $errors, $msg): JsonResponse
    {
        return response()->json([
            'status' => $status,
            'data'=> $data,
            'message' => $msg,
            'errors' => $errors
        ], $status);
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected function paginate($items, $perPage = 5, $page = null, $options = [])
    {
        $numberParameter = config('json-api-paginate.number_parameter');
        $paginationParameter = config('json-api-paginate.pagination_parameter');
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        $paginator = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
        $paginator->setPageName($paginationParameter.'['.$numberParameter.']');
        return $paginator;
    }
}