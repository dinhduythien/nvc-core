<?php

namespace SCart\Core\Api\Middleware;

use Closure;
use Illuminate\Http\Request;

class ApiConnection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!sc_config('api_connection_required')) {
            return $next($request);
        }
        $apiConnection = $request->header('API-CONNECTION');
        $apikey = $request->header('API-KEY');
        if(!$apiConnection || !$apikey) {
            return  response()->json(['error' => 1, 'msg' => 'API-CONNECTION or API-KEY not found']);
        }
        $check = \SCart\Core\Front\Models\ShopApiConnection::check($apiConnection, $apikey);
        if($check) {
            $check->update(['last_active' => date('Y-m-d H:i:s')]);
            $request->merge(['device_type' => $check->apiconnection]);
            return $next($request);
        } else {
            return response()->json(['error' => 1, 'msg' => 'Connection not correct']);
        }
    }
}
