<?php

namespace SCart\Core\Api\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class DeviceTracking
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $deviceId = $request->header('DEVICE-ID');
        if (!empty($deviceId) && $request->user('api')) {
            /** @var \SCart\Core\Library\DeviceTracking $ldt */
            $ldt = App::make('device-tracking');
            $currentDevice = $ldt->findCurrentDevice(true, true);
            $ldt->flagAsVerified($currentDevice, $request->user('api')->id, false, true);
        }
        return $next($request);
    }
}