<?php
namespace SCart\Core\Library;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use SCart\Core\Front\Models\ShopCustomer;
use SCart\Core\Front\Models\ShopDevice;

class DeviceTracking
{
    private $detectData;
    private $currentDevice;
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * retrieve device information from the user-agent string
     * @return array
     * */
    public function detect()
    {
        if (!isset($this->detectData)) {
            $browser = App::make('browser-detect')->detect();
            $isBot = $browser->isBot();
            $family = $browser->browserFamily();
            $platform = $browser->platformFamily();
            $deviceModel = $browser->deviceModel();

            $features = [];

            if ($isBot) {
                $features[] = 'API';
            }
            if ($deviceModel) {
                $features[] = $deviceModel;
            }
            if ($platform) {
                $features[] = $platform;
            }
            if ($family) {
                $features[] = $family;
            }
            if (!empty($this->request->header('API-CONNECTION', ''))) {
                $features[] = $this->request->header('API-CONNECTION');
            }
            // device type is a generated identifier
            // that normally should not change
            $device_type = implode("|", $features);
            // other metadata
            $data = [
                'is_bot' => $isBot,
                'version' => $browser->browserVersion(),
                'engine' => $browser->browserEngine(),
                'platform_family' => $browser->platformFamily(),
                'platform_name' => $browser->platformName(),
                'platform_version' => $browser->platformVersion(),
                'device_model' => $browser->deviceModel(),
                'ip_addresses' => $this->request->ips(),
                'user_agent' => Str::limit($this->request->header('user-agent'), 512),
                'api-connection' => $this->request->header('API-CONNECTION', '')
            ];

            $device_uuid = $this->getDeviceID();
            $this->detectData = compact('device_type', 'data', 'device_uuid');
        }

        return $this->detectData;
    }

    /**
     * @param  string  $deviceId
     * @param  bool  $orNew
     * @param  bool  $update
     *
     * @return ShopDevice|null
     */
    public function findCurrentDevice($deviceId = "", $orNew = false, $update = false): ?ShopDevice
    {
        if (isset($this->currentDevice)) {
            return $this->currentDevice;
        }
        if (empty($deviceId)) {
            $deviceId = $this->getDeviceID();
        }
        $this->currentDevice = ShopDevice::where('device_uuid', '=', $deviceId)->first();

        if (!$this->currentDevice && $orNew) {
            $this->currentDevice = $this->createDeviceFromDetection();
        }

        if ($this->currentDevice && $update) {
            $this->detect();
            $this->currentDevice->ip = $this->request->ip();
            $this->currentDevice->device_type = $this->detectData['device_type'];
            $this->currentDevice->data = array_merge($this->currentDevice->data ?? [], $this->detectData['data']);
            $this->currentDevice->save();
        }

        return $this->currentDevice;
    }

    /**
     * retrieve the device identifier from cookie
     * @return string
     * */
    public function getDeviceID()
    {
        if (!empty($this->request->header('DEVICE-ID', ''))) {
            return $this->request->header('DEVICE-ID');
        }
        return Str::limit($this->request->cookie(config('device-tracking.device_cookie')), 255, '');
    }

    public function findOrSaveDevice(ShopDevice $device, ShopCustomer $user, $registering, $login)
    {
        $data = [];
        if ($registering) {
            $data["registered_at"] = now();
        }
        if ($login) {
            $data["last_login_at"] = now();
        }
        if (!empty($this->request->header('DEVICE-TOKEN', ''))) {
            $data["fcm_token"] = $this->request->header('DEVICE-TOKEN');
        }
        $syncData = [$device->id => $data];
        $user->devices()->sync($syncData, false);
    }

    /**
     * create a new Device instance using detected data
     * @return ShopDevice
     */
    public function createDeviceFromDetection()
    {
        $this->detect();

        $device_uuid = !empty($this->request->header('DEVICE-ID', '')) ? $this->request->header('DEVICE-ID') :
            Str::uuid()->toString() . ':' . Str::random(16);
        $data = $this->detectData['data'];
        $device_type = $this->detectData['device_type'];
        $ip = $this->request->ip();

        return ShopDevice::create(compact('device_uuid', 'data', 'device_type', 'ip'));
    }
}