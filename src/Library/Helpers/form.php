<?php

function render_search_form($action, $keyword = "", $searchPlaceHolder = "", $btnSearchId = "button_search", $btnInputSearch = 'global_filter'): string
{
    if (empty($searchPlaceHolder)) {
        $searchPlaceHolder = sc_language_render('search.placeholder');
    }

    return '<form action="'.$action.'" id="'. $btnSearchId .'">
                <div class="input-field input-group-fields">                    
                    <div class="input-group-wrapper">
                      <i class="material-icons mr-2 search-icon">search</i>
                       <input id="'. $btnInputSearch .'" type="text" name="keyword" class="rounded-0 app-filter" 
                          placeholder="'.$searchPlaceHolder.'" value="'.$keyword.'">     
                    </div>
                    <button type="submit" class="btn btn-large gradient-45deg-green-teal btn-search"><i class="material-icons">search</i></button>
                </div>
            </form>';
}

function render_float_link($action, $cssClasses, $title = "", $materialIcon = ""): string {
    return '<a class="btn-floating '. $cssClasses .'" href="' . $action . '">
                <span title="' . $title . '"><i class="material-icons">'. $materialIcon .'</i></span>
            </a>';
}

function render_float_button_with_onclick($action, $cssClasses, $title = "", $materialIcon = ""): string {
    return '<span class="btn-floating '. $cssClasses .'" onclick="' . $action . '" title="' . $title . '">
                <i class="material-icons">'. $materialIcon .'</i>
            </span>';
}

function render_float_edit_button($action): string {
    return render_float_link($action, 'btn-small cyan',
        sc_language_render('action.edit'), 'edit');
}

function render_float_delete_button($action): string {
    return render_float_button_with_onclick($action, 'btn-small red darken-1 ml-5',
        sc_language_render('action.delete'), 'delete');
}

function render_form_row_one_column($id, $name, $value, $label, $errors, $inputType = 'text', $cssClass = "", $options = []) {
    $cssClass .= " ".$name;
    $errorKey = $options['custom_error_key'] ?? $name;
    $hasError = isset($errors) && $errors->has($errorKey);
    $hasErrorClass = $hasError ? 'has-error '.$errorKey : $errorKey;
    $helpText = $options['help_text'] ?? '';
    $disabled = $options['disabled'] ?? '';
    $readonly = $options['readonly'] ?? '';
    if (!empty($helpText)) {
        $helpText = "<span class='helper-text'>$helpText</span>";
    }
    $icon = '';
    $prefix = $options['prefix'] ?? '';
    $suffix = $options['suffix'] ?? '';
    if (!empty($prefix)) {
        $icon .= '<i class="material-icons prefix">'. $prefix .'</i>';
    }
    if (!empty($suffix)) {
        $icon .= '<i class="material-icons suffix">'. $suffix .'</i>';
    }
    switch ($inputType) {
        case 'textarea':
            $input = '<textarea class="'. $cssClass . '" rows="10" id="'. $id .'" name="'. $name .'" '.$disabled. ' ' . $readonly .'>'. $value .'</textarea>';
            break;
        case 'number':
            $input = '<input type="number" min=0 id="'. $id .'" name="'. $name .'" value="'. $value .'" class="'. $cssClass . '" '.$disabled. ' ' . $readonly .' />';
            break;
        case 'decimal':
            $input = '<input type="number" min=0 step="0.1" id="'. $id .'" name="'. $name .'" value="'. $value .'" class="'. $cssClass . '" '.$disabled. ' ' . $readonly .' />';
            break;
        case 'email':
            $input = '<input type="email" id="'. $id .'" name="'. $name .'" value="'. $value .'" class="'. $cssClass . '" '.$disabled. ' ' . $readonly .' />';
            break;
        case 'password':
            $input = '<input type="password" id="'. $id .'" name="'. $name .'" value="'. $value .'" class="'. $cssClass . '" '.$disabled. ' ' . $readonly .' />';
            break;
        default:
            $input = '<input type="text" id="'. $id .'" name="'. $name .'" value="'. $value .'" class="'. $cssClass . '" '.$disabled. ' ' . $readonly .' />';
    }
    $labelClass = $options['label_class'] ?? '';
    if ($inputType == 'textarea' && stripos($cssClass, 'editor') !== false) {
        $labelClass .= ' editor-label ';
    }
    $errorDisplay = '<small><div class="error">'. $errors->first($errorKey) .'</div></small>';
    $row = '<div class="input-field col s12 '. ($options['container_class'] ?? '') . ' ' . $hasErrorClass .'">' .
                $icon . $input . $helpText.
                '<label for="'. $id .'" class="'. $labelClass .'">'. $label . '</label>';
    if ($hasError) {
        $row .= $errorDisplay;
    }
    $row .= '</div>';
    return $row;
}
