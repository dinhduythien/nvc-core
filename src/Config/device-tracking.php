<?php

return [
    'device_cookie' => 'device_uuid',
    'session_key' => 'nvc-device-tracking',
];