<?php
#S-Cart/Core/Front/Models/ShopProductCategory.php
namespace SCart\Core\Front\Models;

use Illuminate\Database\Eloquent\Model;
use SCart\Core\Library\Traits\HasCompositePrimaryKeyTrait;

class ShopProductCategory extends Model
{
    use HasCompositePrimaryKeyTrait;
    protected $primaryKey = ['category_id', 'product_id'];
    public $incrementing  = false;
    protected $guarded    = [];
    public $timestamps    = false;
    public $table = SC_DB_PREFIX.'shop_product_category';
    protected $connection = SC_CONNECTION;
}
