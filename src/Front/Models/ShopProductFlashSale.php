<?php
#S-Cart/Core/Front/Models/ShopProductDownload.php
namespace SCart\Core\Front\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ShopProductFlashSale extends Model
{
    use ModelTrait;

    public $table = SC_DB_PREFIX.'shop_product_flashsale';
    protected $connection = SC_CONNECTION;

    protected $fillable = ['price', 'product_id', 'discount_string', 'compare_price', 'sold', 'status', 'start_date', 'end_date'];

    public function product()
    {
        return $this->belongsTo(ShopProduct::class, 'product_id', 'id');
    }

    public function isExpired(): bool
    {
        return $this->end_date !== null && $this->end_date < Carbon::now();
    }

    public function isActive(): bool
    {
        return $this->start_date !== null && $this->start_date <= Carbon::now() &&
               ($this->end_date === null || $this->end_date >= Carbon::now());
    }

    public function isFuture(): bool
    {
        return $this->start_date !== null && $this->start_date > Carbon::now();
    }

    public function scopeCurrent($query)
    {
        $now = Carbon::now();
        return $query->where('status', '=', 1)->where('start_date', '<=', $now)->where(function($q) use($now){
            $q->whereNull('end_date')->orWhere('end_date', '>=', $now);
        })->orderBy('start_date', 'desc');
    }

    public function scopeExpired($query)
    {
        return $query->where('end_date', '<', Carbon::now());
    }

    public function scopeFuture($query)
    {
        return $query->where('start_date', '>', Carbon::now());
    }
}
