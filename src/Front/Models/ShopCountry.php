<?php
namespace SCart\Core\Front\Models;

use Illuminate\Database\Eloquent\Model;
use Cache;

class ShopCountry extends Model
{
    public $table = SC_DB_PREFIX.'shop_country';
    public $timestamps               = false;
    private static $getListCountries = null;
    private static $getCodeAll = null;
    private static $defaultCountries = null;
    protected $connection = SC_CONNECTION;

    public static function getListAll()
    {
        if (sc_config_global('cache_status') && sc_config_global('cache_country')) {
            if (!Cache::has('cache_country_list')) {
                if (self::$getListCountries === null) {
                    self::$getListCountries = self::get()->keyBy('code');
                }
                sc_set_cache('cache_country_list', self::$getListCountries);
            }
            return Cache::get('cache_country_list');
        } else {
            if (self::$getListCountries === null) {
                self::$getListCountries = self::get()->keyBy('code');
            }
            return self::$getListCountries;
        }
    }

    public static function getCodeAll()
    {
        if (sc_config_global('cache_status') && sc_config_global('cache_country')) {
            if (!Cache::has('cache_country')) {
                if (self::$getCodeAll === null) {
                    self::$getCodeAll = self::pluck('name', 'code')->all();
                }
                sc_set_cache('cache_country', self::$getCodeAll);
            }
            return Cache::get('cache_country');
        } else {
            if (self::$getCodeAll === null) {
                self::$getCodeAll = self::pluck('name', 'code')->all();
            }
            return self::$getCodeAll;
        }
    }

    public static function getDefaultCountry(){
        if (sc_config_global('cache_status') && sc_config_global('cache_country')) {
            if (!Cache::has('cache_default_country')) {
                if (self::$defaultCountries === null) {
                    self::$defaultCountries = (new self)->where('code', 'VN');
                }
                sc_set_cache('cache_default_country', self::$defaultCountries);
            }
            return Cache::get('cache_default_country');
        } else {
            if (self::$defaultCountries === null) {
                self::$defaultCountries = (new self)->where('code', 'VN');
            }
            return self::$defaultCountries;
        }
    }
}
