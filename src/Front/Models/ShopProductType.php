<?php
namespace SCart\Core\Front\Models;

use Illuminate\Database\Eloquent\Model;

class ShopProductType extends Model
{
    public $timestamps  = false;
    public $table = SC_DB_PREFIX.'shop_product_type';
    protected $guarded   = [];
    protected $connection = SC_CONNECTION;
}