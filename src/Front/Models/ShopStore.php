<?php
namespace SCart\Core\Front\Models;

use Illuminate\Support\Facades\Cache;
use SCart\Core\Admin\Models\AdminConfig;
use SCart\Core\Admin\Models\AdminUserStore;
use Illuminate\Database\Eloquent\Model;
class ShopStore extends Model
{
    public $timestamps = false;
    public $table = SC_DB_PREFIX.'admin_store';
    protected $guarded = [];
    protected static $getAll = null;
    protected static $getStoreActive = null;
    protected static $getCodeActive = null;
    protected static $getDomainPartner = null;
    protected static $getListAllActive = null;
    protected static $arrayStoreId = null;
    protected $connection = SC_CONNECTION;
    
    public function descriptions()
    {
        return $this->hasMany(ShopStoreDescription::class, 'store_id', 'id');
    }

    public function products()
    {
        return $this->hasMany(ShopProduct::class, 'store_id', 'id');
    }

    public function banners()
    {
        return $this->hasMany(ShopBanner::class, 'store_id', 'id');
    }

    public function news()
    {
        return $this->hasMany(ShopNews::class, 'store_id', 'id');
    }

    public function pages()
    {
        return $this->hasMany(ShopPage::class, 'store_id', 'id');
    }


    protected static function boot()
    {
        parent::boot();
        // before delete() method call this
        static::deleting(function ($store) {
            //Store id 1 is default
            if ($store->id == SC_ID_ROOT) {
                return false;
            }
            //Delete store descrition
            $store->descriptions()->delete();
            $store->news()->delete();
            $store->banners()->delete();
            $store->pages()->delete();
            AdminConfig::where('store_id', $store->id)->delete();
            AdminUserStore::where('store_id', $store->id)->delete();
        });
    }


    /**
     * [getAll description]
     *
     * @return  [type]  [return description]
     */
    public static function getListAll()
    {
        if (!Cache::has('cache_list_all_stores')) {
            if (self::$getAll === null) {
                self::$getAll = self::with('descriptions')
                                    ->get()
                                    ->keyBy('id');
            }
            sc_set_cache('cache_list_all_stores', self::$getAll);
        }

        return Cache::get('cache_list_all_stores');
    }

    /**
     * [getAll active description]
     *
     * @return  [type]  [return description]
     */
    public static function getListAllActive()
    {
        if (!Cache::has('cache_list_active_stores')) {
            if (self::$getListAllActive === null) {
                self::$getListAllActive = self::with('descriptions')
                                              ->where('active', 1)
                                              ->get()
                                              ->keyBy('id');
            }
            sc_set_cache('cache_list_active_stores', self::$getListAllActive);
        }

        return Cache::get('cache_list_active_stores');
    }


    /**
     * Get all domain and id store unlock domain
     *
     * @return  [array]  [return description]
     */
    public static function getDomainPartner()
    {
        if (self::$getDomainPartner === null) {
            self::$getDomainPartner = self::where('partner', 1)
                ->whereNotNull('domain') 
                ->pluck('domain', 'id')
                ->all();
        }
        return self::$getDomainPartner;
    }
    

    /**
     * Get all domain and id store active
     *
     * @return  [array]  [return description]
     */
    public static function getStoreActive()
    {
        if (!Cache::has('cache_active_store')) {
            if (self::$getStoreActive === null) {
                self::$getStoreActive = self::where('active', 1)
                                            ->pluck('domain', 'id')
                                            ->all();
            }
            sc_set_cache('cache_active_store', self::$getStoreActive);
        }

        return Cache::get('cache_active_store');
    }
    

    /**
     * Get all code and id store active
     *
     * @return  [array]  [return description]
     */
    public static function getCodeActive()
    {
        if (!Cache::has('cache_active_store_code')) {
            if (self::$getCodeActive === null) {
                self::$getCodeActive = self::where('active', 1)
                                           ->pluck('code', 'id')
                                           ->all();
            }
            sc_set_cache('cache_active_store_code', self::$getCodeActive);
        }

        return Cache::get('cache_active_store_code');
    }

    /**
     * Get array store ID
     *
     * @return array
     */
    public static function getArrayStoreId()
    {
        if (self::$arrayStoreId === null) {
            self::$arrayStoreId = self::pluck('id')->all();
        }
        return self::$arrayStoreId;
    }

    //Function get text description 
    public function getText() {
        return $this->descriptions()->where('lang', sc_get_locale())->first();
    }
    public function getTitle() {
        return $this->getText()->title;
    }
    public function getDescription() {
        return $this->getText()->description;
    }
    public function getKeyword() {
        return $this->getText()->keyword;
    }
    //End  get text description

}
