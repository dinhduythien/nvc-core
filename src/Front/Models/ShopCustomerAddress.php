<?php
#S-Cart/Core/Front/Models/ShopCustomerAddress.php
namespace SCart\Core\Front\Models;

use App\Models\AddressUnit;
use Illuminate\Database\Eloquent\Model;

class ShopCustomerAddress extends Model
{
    protected $guarded    = [];
    public $timestamps    = false;
    public $table = SC_DB_PREFIX.'shop_customer_address';
    protected $connection = SC_CONNECTION;

    public const ADDRESS_TYPE_HOME = 1;
    public const ADDRESS_TYPE_COMPANY = 2;
    public const ADDRESS_TYPE_OTHER = 3;

    public const ADDRESS_TYPES = [
        self::ADDRESS_TYPE_HOME => 'customer.address_type.home',
        self::ADDRESS_TYPE_COMPANY => 'customer.address_type.company',
        self::ADDRESS_TYPE_OTHER => 'customer.sex_other'
    ];

    protected static function boot()
    {
        parent::boot();
        static::updated(function($address){
            if ($address->is_default) {
                $customer = ShopCustomer::find($address->customer_id);
                if ($customer) {
                    $addressParts = AddressUnit::fetchAddressDetail($address->ward_id);
                    $district = $addressParts["district"]["name"];
                    $province = $addressParts["province"]["name"];
                    $customer->update([
                        'address_id' => $address->id,
                        'address1' => $province,
                        'address2' => $district,
                        'address3' => $address->address3
                    ]);
                }
            }
        });
    }
}
