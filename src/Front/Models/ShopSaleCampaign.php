<?php
namespace SCart\Core\Front\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class ShopSaleCampaign extends Model
{
    use ModelTrait;

    const CAMPAIGN_TYPE_ANY = 'ALL';
    const CAMPAIGN_TYPE_CAMPAIGNS = 'CAMPAIGN';
    const CAMPAIGN_TYPE_FLASH_SALES = 'FLASH_SALES';

    const CAMPAIGN_TYPES = [
        self::CAMPAIGN_TYPE_ANY => "All",
        self::CAMPAIGN_TYPE_CAMPAIGNS => "Marketing Campaign",
        self::CAMPAIGN_TYPE_FLASH_SALES => "Flash Sale"
    ];

    public $table = SC_DB_PREFIX.'shop_sale_campaign';
    protected $guarded = [];
    protected $connection = SC_CONNECTION;
    protected $casts = [
        'custom_data' => 'array',
    ];
    protected $fillable = ['name', 'slug', 'title', 'subtitle', 'description', 'sale_campaign_group_id',
        'custom_data', 'active', 'available_at', 'closed_at', 'campaign_type', 'sort'];

    public function banners()
    {
        return $this->belongsToMany(ShopBanner::class, SC_DB_PREFIX.'shop_sale_campaign_banner', 'sale_campaign_id', 'banner_id')
                    ->withPivot('position', 'banner_link', 'active')
                    ->withTimestamps();
    }

    public function activeBanners() {
        return $this->banners()->where('status', '=', 1);
    }

    public function products()
    {
        return $this->belongsToMany(ShopProduct::class, SC_DB_PREFIX.'shop_sale_campaign_product', 'sale_campaign_id', 'product_id')
                    ->withPivot('position', 'price', 'original_price', 'discount_string',
                        'available_stock', 'stock_limit', 'custom_data', 'active')
                    ->withTimestamps();
    }

    public function categories()
    {
        $categories = $this->products()->get()->map(function($product){
            return $product->categories()->limit(1)->pluck('id');
        })->toArray();
        return Arr::flatten($categories);
    }

    public function campaignGroup()
    {
        return $this->belongsTo(ShopSaleCampaignGroup::class, 'sale_campaign_group_id');
    }

    public function defaultBanner()
    {
        return $this->activeBanners()->first() ?? null;
    }

    public function scopeActive($query)
    {
        return $query->where($this->table.'.active', true);
    }

    public function scopeCurrent($query)
    {
        $now = Carbon::now();
        return $query->where('available_at', '<=', $now)->where(function($q) use($now){
            $q->whereNull('closed_at')->orWhere('closed_at', '>=', $now);
        })->orderBy('available_at', 'desc');
    }

    public function scopeFlashSales($query)
    {
        return $query->whereIn('campaign_type', [self::CAMPAIGN_TYPE_FLASH_SALES, self::CAMPAIGN_TYPE_ANY]);
    }

    public function scopeCampaigns($query)
    {
        return $query->whereIn('campaign_type', [self::CAMPAIGN_TYPE_CAMPAIGNS, self::CAMPAIGN_TYPE_ANY]);
    }

    protected static function boot()
    {
        parent::boot();

        // before delete() method call this
        static::deleting(function ($campaign) {
            $campaign->products()->detach();
        });
    }
}