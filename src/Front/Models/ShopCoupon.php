<?php
namespace SCart\Core\Front\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ShopCoupon extends Model
{
    protected $table = SC_DB_PREFIX.'shop_coupon';
    public $incrementing = false;
    protected $primaryKey = 'coupon_key';
    protected $keyType = 'string';
    protected $connection = SC_CONNECTION;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'array',
        'amount' => 'integer',
        'max_amount' => 'integer',
        'min_value' => 'integer',
    ];

    protected $fillable = [
        'coupon_key', 'title', 'description', 'content', 'discount_type', 'apply_per', 'amount',
        'active', 'max_amount', 'start_date', 'end_date', 'min_value', 'max_coupons', 'min_quantity',
        'number_per_user',
        'options',
    ];

    public const DISCOUNT_TYPE_FIXED_AMOUNT = 1;
    public const DISCOUNT_TYPE_PERCENTAGE = 2;
    public const APPLY_PER_ORDER_TOTAL = 1;
    public const APPLY_PER_SHIPPING_FEE = 2;

    public static function discountTypeOptions()
    {
        return [
            self::DISCOUNT_TYPE_FIXED_AMOUNT => 'Số tiền cố định',
            self::DISCOUNT_TYPE_PERCENTAGE => '% giá trị',
        ];
    }

    public static function applyOptions()
    {
        return [
            self::APPLY_PER_ORDER_TOTAL => 'Giá trị đơn hàng',
            self::APPLY_PER_SHIPPING_FEE => 'Phí giao hàng',
        ];
    }

    public function orders()
    {
        return $this->hasMany(ShopOrder::class, 'coupon_key', 'coupon_key');
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeCurrent($query)
    {
        $now = Carbon::now();
        return $query->where(function($q){
            $q->whereNull('start_date')->whereNull('end_date');
        })->orWhere(function($q) use($now) {
            $q->whereNotNull('start_date')->where('start_date', '<=', $now);
            $q->where(function($qr) use($now) { $qr->whereNull('end_date')->orWhere('end_date', '>=', $now); });
        })->orderBy('created_at', 'desc');
    }

    public function calculateDiscountValue(int $originalValue)
    {
        if ($this->discount_type == self::DISCOUNT_TYPE_FIXED_AMOUNT) {
            return $this->amount;
        } else {
            $discount = $this->amount/100 * $originalValue;
            if ($discount >= $this->max_amount) {
                $discount = $this->max_amount;
            }
            return $discount;
        }
    }
}