<?php

namespace SCart\Core\Front\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopDevice extends Model
{
    use SoftDeletes;
    use ModelTrait;

    public $table = SC_DB_PREFIX.'shop_device';
    protected $guarded = [];
    protected $connection = SC_CONNECTION;
    protected $casts = [
        'data' => 'array',
    ];
    protected $fillable = ['device_uuid', 'device_type', 'data', 'ip'];
}