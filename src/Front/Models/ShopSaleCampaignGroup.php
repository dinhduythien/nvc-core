<?php
namespace SCart\Core\Front\Models;

use Illuminate\Database\Eloquent\Model;

class ShopSaleCampaignGroup extends Model
{
    use ModelTrait;

    const GROUP_TYPE_ANY = 'ALL';
    const GROUP_TYPE_CAMPAIGNS = 'CAMPAIGN';
    const GROUP_TYPE_FLASH_SALES = 'FLASH_SALES';

    const GROUP_TYPE_TYPES = [
        self::GROUP_TYPE_ANY => 'All',
        self::GROUP_TYPE_CAMPAIGNS => "Marketing Campaign",
        self::GROUP_TYPE_FLASH_SALES => "Flash Sale"
    ];

    const GROUP_DISPLAY_TYPE_BLOCK = 'BLOCK';
    const GROUP_DISPLAY_TYPE_SLIDER = 'SLIDER';

    const GROUP_DISPLAY_TYPES = [
        self::GROUP_DISPLAY_TYPE_BLOCK => '2 cột',
        self::GROUP_DISPLAY_TYPE_SLIDER => 'Slider'
    ];

    public $table = SC_DB_PREFIX.'shop_sale_campaign_group';
    protected $guarded = [];
    protected $connection = SC_CONNECTION;

    protected $fillable = ['name', 'subtitle', 'active', 'image', 'description', 'group_type', 'banner_id', 'sort', 'display_type'];

    /*
    Get thumb
    */
    public function getThumb()
    {
        return sc_image_get_path_thumb($this->image);
    }

    public function campaigns()
    {
        return $this->hasMany(ShopSaleCampaign::class, 'sale_campaign_group_id');
    }

    public function banner() {
        return $this->belongsTo(ShopBanner::class, 'banner_id');
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeFlashSales($query)
    {
        return $query->where('group_type', self::GROUP_TYPE_FLASH_SALES);
    }

    public function scopeCampaigns($query)
    {
        return $query->where('group_type', self::GROUP_TYPE_CAMPAIGNS);
    }

    protected static function boot()
    {
        parent::boot();
        self::deleted(function($shopSaleCampaignGroup){
            $shopSaleCampaignGroup->campaigns()->update(['sale_campaign_group_id' => null]);
        });
    }
}