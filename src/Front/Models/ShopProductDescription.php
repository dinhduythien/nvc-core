<?php
#S-Cart/Core/Front/Models/ShopProductDescription.php
namespace SCart\Core\Front\Models;

use Illuminate\Database\Eloquent\Model;
use SCart\Core\Library\Traits\HasCompositePrimaryKeyTrait;

class ShopProductDescription extends Model
{
    use HasCompositePrimaryKeyTrait;

    protected $primaryKey = ['lang', 'product_id'];
    public $incrementing  = false;
    protected $guarded    = [];
    public $timestamps    = false;
    public $table = SC_DB_PREFIX.'shop_product_description';
    protected $connection = SC_CONNECTION;
}
