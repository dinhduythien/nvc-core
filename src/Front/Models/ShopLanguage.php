<?php
#S-Cart/Core/Front/Models/ShopLanguage.php
namespace SCart\Core\Front\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class ShopLanguage extends Model
{
    public $table = SC_DB_PREFIX.'shop_language';
    public $timestamps                = false;
    protected $guarded                = [];
    private static $getListAll      = null;
    private static $getListActive      = null;
    private static $getArrayLanguages = null;
    private static $getCodeActive = null;
    protected $connection = SC_CONNECTION;

    public static function getListAll()
    {
        if (!Cache::has('cache_all_languages')) {
            if (self::$getListAll === null) {
                self::$getListAll = self::get()
                                        ->keyBy('code');
            }
            sc_set_cache('cache_all_languages', self::$getListAll);
        }

        return Cache::get('cache_all_languages');
    }

    public static function getListActive()
    {
        if (!Cache::has('cache_default_language')) {
            if (self::$getListActive === null) {
                self::$getListActive = self::where('status', 1)
                                           ->get()
                                           ->keyBy('code');
            }
            sc_set_cache('cache_default_language', self::$getListActive);
        }

        return Cache::get('cache_default_language');
    }

    public static function getCodeActive()
    {
        if (!Cache::has('cache_default_language_code')) {
            if (self::$getCodeActive === null) {
                self::$getCodeActive = self::where('status', 1)
                                           ->pluck('name', 'code')
                                           ->all();
            }
            sc_set_cache('cache_default_language_code', self::$getCodeActive);
        }

        return Cache::get('cache_default_language_code');
    }

    public static function getCodeAll()
    {
        if (!Cache::has('cache_default_language_codes')) {
            if (self::$getArrayLanguages === null) {
                self::$getArrayLanguages = self::pluck('name', 'code')->all();
            }
            sc_set_cache('cache_default_language_codes', self::$getArrayLanguages);
        }

        return Cache::get('cache_default_language_codes');
    }
    
    protected static function boot() {
        parent::boot();
        static::deleting(function ($model) {
            if (in_array($model->id, SC_GUARD_LANGUAGE)) {
                return false;
            }
        });
    }
}
