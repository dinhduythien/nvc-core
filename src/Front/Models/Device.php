<?php

namespace SCart\Core\Front\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Device extends Model
{
    use SoftDeletes;
    use ModelTrait;
    protected static $class;

    public $table = SC_DB_PREFIX.'devices';
    protected $guarded = [];
    protected $connection = SC_CONNECTION;
    protected $casts = [
        'data' => 'array',
    ];


    public function touch()
    {
        $this->{static::UPDATED_AT} = now();
    }
    /**
     * @return string user class fqn
     */
    public static function getUserClass()
    {
        if (isset(static::$class)) {
            return static::$class;
        }

        $u = config('auth.device_tracking.user_model');

        if (!$u) {
            if (class_exists("App\\Models\\User")) {
                $u = "App\\Models\\User";
            } else if (class_exists("App\\User")) {
                $u = "App\\User";
            }
        }

        if (!class_exists($u)) {
            throw new HttpException(500, "class $u not found");
        }

        if (!is_subclass_of($u, Model::class)) {
            throw new HttpException(500, "class $u is not model");
        }
        static::$class = $u;

        return $u;
    }

    public function user()
    {
        return $this->belongsToMany(static::getUserClass(), 'device_user')
                    ->using(DeviceUser::class)
                    ->withPivot('verified_at')->withTimestamps();
    }

    public function pivot()
    {
        return $this->hasMany(DeviceUser::class);
    }

    public function currentUserStatus($userId)
    {
        return $this->hasOne(DeviceUser::class)
                    ->where('user_id', '=', $userId);
    }

    public function isUsedBy($user_id)
    {
        $count = $this->user()
                      ->where('device_user.user_id',$user_id)->count();

        return $count > 0;
    }
}