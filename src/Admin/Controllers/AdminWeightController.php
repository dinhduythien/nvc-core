<?php
namespace SCart\Core\Admin\Controllers;

use App\Http\Controllers\RootAdminController;
use SCart\Core\Front\Models\ShopWeight;
use Validator;

class AdminWeightController extends RootAdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        $data = [
            'title' => sc_language_render('admin.weight.list'),
            'title_action' => '<i class="fa fa-plus" aria-hidden="true"></i> ' . sc_language_render('admin.weight.add_new_title'),
            'subTitle' => '',
            'icon' => 'fa fa-indent',
            'urlDeleteItem' => sc_route_admin('admin_weight_unit.delete'),
            'removeList' => 0, // 1 - Enable function delete list item
            'buttonRefresh' => 0, // 1 - Enable button refresh
            'buttonSort' => 0, // 1 - Enable button sort
            'css' => '', 
            'js' => '',
            'url_action' => sc_route_admin('admin_weight_unit.create'),
        ];

        $listTh = [
            'id' => 'ID',
            'name' => sc_language_render('admin.weight.name'),
            'description' => sc_language_render('admin.weight.description'),
            'action' => sc_language_render('action.title'),
        ];
        $obj = new ShopWeight;
        $obj = $obj->orderBy('id', 'desc');
        $dataTmp = $obj->paginate(20);

        $dataTr = [];
        foreach ($dataTmp as $key => $row) {
            $dataTr[] = [
                'id' => $row['id'],
                'name' => $row['name'],
                'description' => $row['description'],
                'action' => '
                    <a class="btn-floating btn-small cyan" href="' . sc_route_admin('admin_weight_unit.edit', ['id' => $row['id']]) . '"><span title="' . sc_language_render('action.edit') . '"><i class="material-icons">edit</i></span></a>
                    <a class="btn-floating btn-small red darken-1" href="javascript:void(0)" onclick="deleteItem(' . $row['id'] . ')"><span title="' . sc_language_render('action.delete') . '"><i class="material-icons">delete</i></span></a>'
                ,
            ];
        }

        $data['listTh'] = $listTh;
        $data['dataTr'] = $dataTr;
        $data['pagination'] = $dataTmp->appends(request()->except(['_token', '_pjax']))->links($this->templatePathAdmin.'component.pagination');
        $data['resultItems'] = sc_language_render('admin.result_item', ['item_from' => $dataTmp->firstItem(), 'item_to' => $dataTmp->lastItem(), 'total' =>  $dataTmp->total()]);

        $data['layout'] = 'index';
        return view($this->templatePathAdmin.'screen.weight')->with($data);
    }

/**
 * Post create new item in admin
 * @return [type] [description]
 */
    public function postCreate()
    {
        $data = request()->all();
        $dataOrigin = request()->all();
        $validator = Validator::make($dataOrigin, [
            'name' => 'required|unique:"'.ShopWeight::class.'",name',
            'description' => 'required',
        ], [
            'name.required' => sc_language_render('validation.required'),
        ]);

        if ($validator->fails()) {
            // dd($validator->messages());
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
//Create new order
        $dataInsert = [
            'name' => $data['name'],
            'description' => $data['description'],
        ];
        $obj = ShopWeight::create($dataInsert);
//
        return redirect()->route('admin_weight_unit.index')->with('success', sc_language_render('action.create_success'));

    }

/**
 * Form edit
 */
public function edit($id)
{
    $weight = ShopWeight::find($id);
    if(!$weight) {
        return 'No data';
    }
    $data = [
        'title' => sc_language_render('admin.weight.list'),
        'title_action' => '<i class="fa fa-edit" aria-hidden="true"></i> ' . sc_language_render('action.edit'),
        'subTitle' => '',
        'icon' => 'fa fa-indent',
        'urlDeleteItem' => sc_route_admin('admin_weight_unit.delete'),
        'removeList' => 0, // 1 - Enable function delete list item
        'buttonRefresh' => 0, // 1 - Enable button refresh
        'buttonSort' => 0, // 1 - Enable button sort
        'css' => '', 
        'js' => '',
        'url_action' => sc_route_admin('admin_weight_unit.edit', ['id' => $weight['id']]),
        'weight' => $weight,
        'id' => $id,
    ];

    $listTh = [
        'id' => 'ID',
        'name' => sc_language_render('admin.weight.name'),
        'description' => sc_language_render('admin.weight.description'),
        'action' => sc_language_render('action.title'),
    ];
    $obj = new ShopWeight;
    $obj = $obj->orderBy('id', 'desc');
    $dataTmp = $obj->paginate(20);

    $dataTr = [];
    foreach ($dataTmp as $key => $row) {
        $dataTr[] = [
            'id' => $row['id'],
            'name' => $row['name'],
            'description' => $row['description'],
            'action' => '
                    <a class="btn-floating btn-small cyan" href="' . sc_route_admin('admin_weight_unit.edit', ['id' => $row['id']]) . '"><span title="' . sc_language_render('action.edit') . '"><i class="material-icons">edit</i></span></a>
                    <a class="btn-floating btn-small red darken-1" href="javascript:void(0)" onclick="deleteItem(' . $row['id'] . ')"><span title="' . sc_language_render('action.delete') . '"><i class="material-icons">delete</i></span></a>'
            ,
        ];
    }

    $data['listTh'] = $listTh;
    $data['dataTr'] = $dataTr;
    $data['pagination'] = $dataTmp->appends(request()->except(['_token', '_pjax']))->links($this->templatePathAdmin.'component.pagination');
    $data['resultItems'] = sc_language_render('admin.result_item', ['item_from' => $dataTmp->firstItem(), 'item_to' => $dataTmp->lastItem(), 'total' =>  $dataTmp->total()]);

    $data['layout'] = 'edit';
    return view($this->templatePathAdmin.'screen.weight')
        ->with($data);
}

/**
 * update status
 */
    public function postEdit($id)
    {
        $data = request()->all();
        $dataOrigin = request()->all();
        $obj = ShopWeight::find($id);
        $validator = Validator::make($dataOrigin, [
            'name' => 'required|unique:"'.ShopWeight::class.'",name,' . $obj->id . ',id',
            'description' => 'required',
        ], [
            'name.required' => sc_language_render('validation.required'),
        ]);

        if ($validator->fails()) {
            // dd($validator->messages());
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
//Edit
        $dataUpdate = [
            'name' => $data['name'],
            'description' => $data['description'],
        ];
        $obj->update($dataUpdate);

//
        return redirect()->back()->with('success', sc_language_render('admin.edit_success'));

    }

/*
Delete list item
Need mothod destroy to boot deleting in model
 */
    public function deleteList()
    {
        if (!request()->ajax()) {
            return response()->json(['error' => 1, 'msg' => sc_language_render('admin.method_not_allow')]);
        } else {
            $ids = request('ids');
            $arrID = explode(',', $ids);
            ShopWeight::destroy($arrID);
            return response()->json(['error' => 0, 'msg' => '']);
        }
    }

}
