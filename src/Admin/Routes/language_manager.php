<?php
Route::group(['prefix' => 'language_manager'], function () {
    Route::get('/', 'AdminLanguageManagerController@index')->name('admin_language_manager.index');
    Route::post('/create', 'AdminLanguageManagerController@postCreate')->name('admin_language_manager.create');
    Route::post('/update', 'AdminLanguageManagerController@postUpdate')->name('admin_language_manager.update');
});
